<?php
    include_once('application/config/global.config.php');
    header('Cache-Control: no-cache, no-store, must-revalidate');               // HTTP 1.1 Cache Disabling
    header('Pragma: no-cache');                                                 // HTTP 1.0  Cache Disabling
    header('Expires: 0');                                                       // Proxies Cache Disabling (Probably unnecessary)
?>
<!html>
<html>
    <head>
        <title>Weather for <?php echo WEATHER_CITY . ', ' . WEATHER_STATE ?></title>
        <meta name="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=0.75" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="author" href="<?php echo SITE_HOME ?>/humans.txt" type="text/plain" />
        <link rel="stylesheet" href="<?php echo SITE_HOME ?>/includes/css/reset.css" />
        <link rel="stylesheet" href="<?php echo SITE_HOME ?>/includes/css/style.css" />
        <script src="<?php echo SITE_HOME ?>/includes/js/zepto.min.js" defer></script>
        <script src="<?php echo SITE_HOME ?>/includes/js/global.min.js" defer></script>
        <meta name="x-timestamp-page" content="<?php echo date('U') ?>" />
        <meta name="x-timestamp-jquery" content="null" />
        <meta name="x-timestamp-database" content="null" />
    </head>
    <body>
        <div id="container">
            <!-- Interior Conditions -->
            <div id="inside" class="col">
                <div id="inside_title" class="row-half header" title="">INSIDE</div>
                <div class="row-full">
                    <span id="inside_temp0">&nbsp;</span>
                    <span id="inside_temp0A">&nbsp;</span>
                </div>
                <div class="row-full">
                    <span id="outside_pressure">&nbsp;</span>
                    <span id="outside_pressuretype"></span>
                </div>
                <div class="row-full">
                    <span id="time">&nbsp;</span>
                    <span id="date">&nbsp;</span>
                </div>
            </div>
            <!-- Exterior Conditions -->
            <div id="outside" class="col">
                <div id="outside_title" title="" class="row-half header">OUTSIDE</div>
                <div class="row-full">
                    <span id="outside_temp0">&nbsp;</span>
                    <span id="outside_temp0A">&nbsp;</span>
                </div>
                <div class="row-half conditions">
                    <span id="outside_sunrise" title="Sunrise">00:00</span>
                    <span id="outside_sunicon" class="icon ui sunrise"></span>
                    <span id="outside_sunset" title="Sunset">00:00</span>
                </div>
                <div class="row-half conditions">
                    <span id="outside_wind0">Wind</span>
                    <span id="outside_windicon" class="icon ui wind"></span>
                    <span id="outside_wind0A">
                        <div id="wind_speed"></div>
                        <div id="wind_dir"></div>
                    </span>
                </div>
                <div class="row-half">
                    <span id="outside_temp1">&nbsp;</span>
                    <span id="outside_temp1A">&nbsp;</span>
                </div>
                <div class="row-half">
                    <span id="outside_temp2">&nbsp;</span>
                    <span id="outside_temp2A">&nbsp;</span>
                </div>
            </div>
            <!-- Forecast -->
            <div id="forecast" class="col">
                <div id="forecast_title" title="" class="row-half header">FORECAST</div>
                <div class="row-full">
                    <div id="forecast1_icon" class="icon blank"></div>
                    <div class="fore-col">
                        <span id="forecast1_day" class="day">&nbsp;</span>
                        <div class="fore-detail fore-temp left">
                            <span>High:<br />Low:<br />Precipitation:</span>
                        </div>
                        <div class="fore-detail fore-temp right">
                            <span id="forecast1_details">&nbsp;<br />&nbsp;<br />&nbsp;</span>
                        </div>
                    </div>
                    <span id="forecast1_condition" class="fore-text">&nbsp;</span>
                </div>
                <div class="row-full">
                    <div id="forecast2_icon" class="icon blank"></div>
                    <div class="fore-col">
                        <span id="forecast2_day" class="day">&nbsp;</span>
                        <div class="fore-detail fore-temp left">
                            <span>High:<br />Low:<br />Precipitation:</span>
                        </div>
                        <div class="fore-detail fore-temp right">
                            <span id="forecast2_details">&nbsp;<br />&nbsp;<br />&nbsp;</span>
                        </div>
                    </div>
                    <span id="forecast2_condition" class="fore-text">&nbsp;</span>
                </div>
                <div class="row-full">
                    <div id="forecast3_icon" class="icon blank"></div>
                    <div class="fore-col">
                        <span id="forecast3_day" class="day">&nbsp;</span>
                        <div class="fore-detail fore-temp left">
                            <span>High:<br />Low:<br />Precipitation:</span>
                        </div>
                        <div class="fore-detail fore-temp right">
                            <span id="forecast3_details">&nbsp;<br />&nbsp;<br />&nbsp;</span>
                        </div>
                    </div>
                    <span id="forecast3_condition" class="fore-text">&nbsp;</span>
                </div>
            </div>
        </div>
    </body>
</html>