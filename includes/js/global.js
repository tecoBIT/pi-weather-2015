$(document).ready(function(){
    //Variables
    var bFirstLoad = true;                                                      //First load? Force redraw
    var bIsDate = true;                                                         //Keep track if we're showing date or note
    var now = '';                                                               //Used for Date objects
    var oIntervalDelay = "";                                                    //Used as a setInterval Holder for SpecialDays
    var oIntervalMessage = "";                                                  //Used as a setInterval Holder for UpdateSpecialMessages
    var sMessage = "";                                                          //Store today's special message
    var sDateTransition = "anim_fade";                                          //Class used to animate transitions
    var iMinAge = 3600000;                                                      //Minimum Age for data to be considered "Fresh"
    var iPressureLastUpdated = 0;                                               //Remember last time pressure icon was updated
    var iPressureLastValue = 0;                                                 //Remember last pressure (to tell if up/down/same)
    var iTempInsideLastUpdated = 0;                                             //Remember the last time inside temps were updated
    var iTempInsideLastValue = 0;                                               //Remember the last temperature inside
    var iTempOutsideLastUpdated = 0;                                            //Remember the last time outside temps were updated
    var iTempOutsideLastValue = 0;                                              //Remember the last temperature outside
    var iHourlyForecastOffSetHours = 6;                                         //The Interval between hourly forecast displays (default: 2 hours)

    //Forcefully disable AJAX Caching (Incase Pi Cache's JSON responses)
    //$.ajaxSetup({ cache: false });

//FUNCTIONS ====================================================================
    //DEBUG --------------------------------------------------------------------
    //  Console debugging when needed. Only echos if global debug = true
    function debug(sInput) {
        console.log("["+(new Date).toLocaleTimeString()+"] "+sInput);
    }

    //CHECK ARROW STATUS -------------------------------------------------------
    //  Compares a current value against a previous value to detect change.
    //  Up/Down arrows remain until the value has stabilized for the duration
    //  of the CurrentTime - PreviousTime > MinAge (Global)
    //  Returns "NA" on first run, instruct calling queries to update as needed
    function getArrowClass(iCurrentValue, iPrevValue,iPrevTime) {
        //debug(">getArrowClass...");
        if(iPrevValue == 0) {
            //debug(">>Update Icon -> First Run");
            return "drawarrow nochange";
        }
        if(iCurrentValue > iPrevValue) {
            //debug(">>Update Icon -> Increased");
            return "drawarrow up";
        } else {
            if(iCurrentValue < iPrevValue) {
                //debug(">>Update Icon -> Decreased");
                return "drawarrow down";
            } else {
                if(((new Date).getTime()-iPrevTime) >= iMinAge){
                    //debug(">>Update Icon -> No Change");
                    return "drawarrow nochange";
                    return "drawarrow nochange";
                } else {
                    //debug(">>DO NOT UPDATE -> Change too soon");
                    return "";
                }
            }
        }
    }

    //FORMATHOURS --------------------------------------------------------------
    //  Take an Epoch DateTime and return it as HHA/P (eg: 09PM)
    function formatHours(iEpoch) {
        //debug("Format Hours...");
        //debug(">Input: "+iEpoch);
        now = new Date(parseInt(iEpoch)*1000);
        if(now.getHours() > 11) {
            if(now.getHours() > 12) {
                return pad(now.getHours()-12) + "PM";
            } else {
                return "12PM";
            }
        } else {
            if(now.getHours() == 0) {
                return "12AM";
            } else {
                return pad(now.getHours()) + "AM";
            }
        }
    }

    //PAD ----------------------------------------------------------------------
    //  Pad out the Hours and Minutes with leading zeros when needed to keep a "00:00" time format
    function pad(sInput){
        //debug('>Padding '+sInput);
        sInput = sInput.toString();
        if(sInput.length < 2){
            return "0"+sInput;
        } else {
            return sInput;
        }
    }

    //Update All ---------------------------------------------------------------
    //  Updates everything when called EXCEPT FOR DATE, that is updated by Time
    function updateAll() {
        updateTime();
        updateInside();
        updateWeatherOutside();
        updateWeatherForecast();
        updateSpecialMessages();
        updateOpacity();
    }

    //Update Date --------------------------------------------------------------
    function updateDate() {
        //debug("Updating Date...");
        aMonths = [ "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];
        aDays = [   "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        now = new Date;
        sDay = now.getDate();
        switch(sDay) {
            case 1:     case 21:    case 31:    sDay = ' ' + sDay + 'st, '; break;
            case 2:     case 22:                sDay = ' ' + sDay + 'nd, '; break;
            case 3:     case 23:                sDay = ' ' + sDay + 'rd, '; break;
            default:                            sDay = ' ' + sDay + 'th, ';
        }
        $('#date').html(aDays[now.getDay()] + ", " + aMonths[now.getMonth()] + sDay + now.getFullYear());
        bIsDate = true;
        //debug(">Date updated.");
        clearInterval(oIntervalDelay);
    }

    //Update Inside ------------------------------------------------------------
    //  Updates the internal temperature & humidity
    function updateInside() {
        var timeNow = (new Date).getTime()/1000;
        //debug('Update Inside');
        $.getJSON('/json/inside.php', function(data) {
            //debug('>Ajax response. Formatting...');
            if((timeNow - data.time) < (iMinAge/1000)) {                        //Compare Data Timestamp against current time
                //debug(">Sensor data is good");
                sAppend = getArrowClass(data.temp,iTempInsideLastValue,iTempInsideLastUpdated);
                if(sAppend == "NA") { iTempLastValue = data.temp; sAppend = ""; }
                $("#inside_temp0").html(data.temp + "&deg;" + data.type);
                $("#inside_temp0A").html(data.humidity + "% Relative Humidity").removeClass();
                updateTimestampTitle('#inside_title',data.time);
            } else {                                                            //Sensor data is Greater than our minimum age, show error
                //debug(">Sensor data is out of date");
                sAppend = "";                                                   //Skip the Icon update, we're going to hide it
                $("#inside_temp0").html("???");                                 //Show invalid data, draw error class
                $("#inside_temp0A").html("Sensor data out of date");
            }
            if(sAppend != "") {                                                 //If Icon changed...
                //debug(">Update Pressure Icon");
                iTempInsideLastValue = data.temp;                               //Make note of previous pressure
                iTempInsideLastUpdated = (new Date).getTime();                  //Make note of hour of this update
                $("#inside_temp0").removeClass().addClass(sAppend);             //Clear existing and add the class that draws the icon
            }
        });
    }

    //Update Opacity -----------------------------------------------------------
    //  Toggles the opacity for the Container Div to psuedo-dim brightness
    function updateOpacity() {
        //debug("Updating Opacity");
        now = new Date;
        $.getJSON('/json/opacity.php', function(data) {
            //debug("> Opacity: " + now.GetHours + " -> " + data[now.getHours()]);
            $("#container").css({opacity: data[now.getHours()]/100});
        });
    }

    //Update Special Display ---------------------------------------------------
    //Updates the message beneat the clock with a special message or date if needed
    function updateSpecialDisplay() {
        //("Update Special Display");
        if(sMessage != "") {
            //debug(">There is a message!");
            $("#date").toggleClass(sDateTransition);
            if(bIsDate == true) {
                //debug(">Date is displayed. Show message");
                oIntervalDelay = setInterval(function(){updateSpecialDisplayDelayed(false)},1001);
                bIsDate = false;
            } else {
                //debug(">Special is displayed. Show date");
                oIntervalDelay = setInterval(function(){updateSpecialDisplayDelayed(true)},1001);
                bIsDate = true;
            }
        } else {
            //debug(">No special message");
            clearInterval(oIntervalMessage);
            clearInterval(oIntervalDelay);
            if(bIsDate == false) {
                //debug(">>Date not shown, one last update");
                oIntervalDelay = setInterval(function(){updateSpecialDisplayDelayed(true)},1001);
            }
        }
    }

    //Update Special Days Delayed ----------------------------------------------
    //A delayed function (Nested setInterval from updateSpecialDays that allows
    //the fade out animation to finish before updating the details & clearing pause
    //NOTE: Using this instead of .delay() as zepto.js does not support .delay()
    function updateSpecialDisplayDelayed(bDrawDate){
        //debug(">Update Special Display (Delayed)");
        if(bDrawDate == false) {
            $("#date").html(sMessage);
        } else {
            updateDate();
        }
        $("#date").toggleClass(sDateTransition);
        clearInterval(oIntervalDelay);
    }

    //Update Special Messages --------------------------------------------------
    //Check the JSON Input to determine if there is a message that should be
    //displayed under the time.  This is triggered along with the updateAll() group
    function updateSpecialMessages() {
        //debug("Update Special Messages");
        $.getJSON('/json/special.php', function(data) {
            if(data.special == true) {
                if(data.note != sMessage) {
                    //debug(">New Message!");
                    sMessage = data.note;
                    clearInterval(oIntervalMessage);
                    oIntervalMessage = setInterval(updateSpecialDisplay,10000);
                } else {
                    //debug(">Message but not new");
                }
            } else {
                //debug(">No message for today");
                sMessage = "";
            }
        });
    }

    //Update Timestamps --------------------------------------------------------
    //Meta writes Epoch values to meta tags for debugging
    function updateTimestampMeta(sMeta,sValue) {
        //debug("Updating Meta value for ["+sMeta+"]");
        $('meta[name='+sMeta+']').remove();
        $('head').append( '<meta name="'+sMeta+'" content="'+sValue+'">' );
    }

    //Update TimestampTitle
    //Title's write human-readable datestamps to column titles as Mouse-over tags
    function updateTimestampTitle(sID,iEpoch) {
        //debug("Updating Cached time for ["+sID+"]");
        now = new Date(1000*iEpoch);
        $(sID).prop('title','Cached on ' + now.toLocaleString());
    }

    //Update Time --------------------------------------------------------------
    function updateTime() {
        //debug('Updating Time...');
        now = new Date;
        if(bFirstLoad == false) {
            if(now.getHours() == 0) {
                if(now.getMinutes() == 0){
                    //debug('>Midnight! Update Date');
                    updateDate();
                    updateSpecialMessages();
                }
            }
        } else {
            bFirstLoad = false;
        }
        $('#time').html(pad(now.getHours()) + ":" + pad(now.getMinutes()));
        updateTimestampMeta('x-timestamp-jquery',Math.floor((new Date).getTime()/1000));
        //debug('>Time Updated');
    }

    //Update Weather Forecast --------------------------------------------------
    function updateWeatherForecast() {
        //debug("Update Weather (Forecast)...");
        $.getJSON('/json/forecast.php', function(data) {
            if(data[0].isToday == true) {
                //debug('>Data is valid');
                //debug('>Ajax Resposne! Formating...');
                for(i=0;i<3;i++) {
                    if(data[i].isToday == true) {                               //If today, change dayname to "Today"
                        $("#forecast"+(i+1)+"_day").html("Today");
                    } else {
                        $("#forecast"+(i+1)+"_day").html(data[i].dayname);
                    }
                    $("#forecast"+(i+1)+"_condition").html(data[i].conditions);
                    $("#forecast"+(i+1)+"_details").html(data[i].temp_high + "&deg;" + data[i].temp_type + "<br />" + data[i].temp_low + "&deg;" + data[i].temp_type + "<br />" + data[i].precipitation+"%");
                    $("#forecast"+(i+1)+"_icon").removeClass().addClass('icon '+data[i].icon);
                }
            } else {                                                            //If the first entry is NOT today, data is outdated
                //debug(">Data is out of date");
                for(i=0;i<3;i++) {
                    $("#forecast"+(i+1)+"_day").html("Unknown");
                    $("#forecast"+(i+1)+"_condition").html("Sensor data out of date");
                    $("#forecast"+(i+1)+"_details").html("--<br />--<br />--");
                    $("#forecast"+(i+1)+"_icon").removeClass().addClass('icon unknown');
                }
            }
        });
    }

    //Update Weather Outside ---------------------------------------------------
    function updateWeatherOutside() {
        //debug("Update Weather (Outside)...");
        var oPressure = $("#outside_pressure");
        var timeNow = (new Date).getTime() / 1000;
        $.getJSON('/json/outside.php', function(data) {
            //debug(">Ajax Response! Formating...");
            if((timeNow - data.current.timestamp) < (iMinAge/1000)) {
                //debug(">Data is valid");
                //debug(">Pressure Icons...");
                iCurrentPressure = parseFloat(data.current.pressure);
                sAppend = getArrowClass(iCurrentPressure,iPressureLastValue,iPressureLastUpdated);
                if(sAppend == "NA") { iPressureLastValue = iCurrentPressure; sAppend = ""; }
                oPressure.html(data.current.pressure).removeClass("error");     //Update Pressure Display
                if(sAppend != "") {                                             //If Icon changed...
                    //debug(">Update Pressure Icon");
                    iPressureLastValue = iCurrentPressure;                      //Make note of previous pressure
                    iPressureLastUpdated = (new Date).getTime();                //Make note of hour of this update
                    oPressure.removeClass().addClass(sAppend);                  //Clear existing and add the class that draws the icon
                }
                $("#outside_pressuretype").html("Barometric Pressure ("+data.current.pressure_type+")");
                //debug(">Temperature Icons...");
                sAppend = getArrowClass(data.current.temperature,iTempOutsideLastValue,iTempOutsideLastUpdated);
                if(sAppend == "NA") { iTempOutsideLastValue = data.current.temperature; sAppend = "";}
                $("#outside_temp0").html(data.current.temperature + "&deg;" + data.current.temp_type);
                if(sAppend != "") {                                             //Update Temperature Arrow Class
                    //debug(">Update Temperature Icon");
                    iTempOutsideLastValue = data.current.temperature;           //Note previous temperature
                    iTempOutsideLastUpdated = (new Date).getTime();             //Note hour of this update
                    $("#outside_temp0").removeClass().addClass(sAppend);        //Clear & redraw the arrows
                }
                $("#outside_temp0A").html("Feels like " + data.hourly[0].temp_feels + "&deg;" + data.hourly[0].temp_type + " &bull; " + data.current.humidity + "% Humidity");
                //debug(">Update Windspeed");
                $("#wind_speed").html(pad(data.hourly[0].wind_speed));
                $("#wind_dir").html(data.hourly[0].wind_type + '<br />' + data.hourly[0].wind_dir);
                //debug(">Update Hourly Forecast");
                for(i=0;i<2;i++) {
                    //debug(">>Updating Hour+"+(i+1));
                    $("#outside_temp"+(i+1)).html(formatHours(data.hourly[(i*iHourlyForecastOffSetHours)].epoch));
                    $("#outside_temp"+(i+1)+"A").html(data.hourly[(i*iHourlyForecastOffSetHours)].temp+"&deg;"+data.hourly[(i*iHourlyForecastOffSetHours)].temp_type);
                }
                updateTimestampMeta('x-timestamp-database',data.current.timestamp);
                updateTimestampTitle('#outside_title',data.current.timestamp);
                updateTimestampTitle('#forecast_title',data.current.timestamp);
                //debug("> Updating Astronomy");
                $("#outside_sunrise").html(pad(data.astronomy.sunriseHour) + ':' + pad(data.astronomy.sunriseMinute));
                $("#outside_sunset").html(pad(data.astronomy.sunsetHour) + ':' + pad(data.astronomy.sunsetMinute));
            } else {                                                            //Data is out of date
                //debug(">Data is out of date");
                oPressure.html("???");
                $("#outside_pressuretype").html("Sensor data out of date");
                $("#outside_temp0").html("???");
                $("#outside_temp0A").html("Sensor data out of date");
                for(i=0;i<2;i++) {
                    $("#outside_temp"+(i+1)).html("--");
                    $("#outside_temp"+(i+1)+"A").html("???");
                }
            }
        });
    }

//TRIGGERS =====================================================================
    //First Load Updates
    updateAll();
    updateDate();

    //Scheduled & Repetitive Updates (In Milliseconds)
    setInterval(updateAll,60000);                                               //Every 60 seconds, update time/weather/etc
});