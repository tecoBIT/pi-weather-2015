<?php
/* *****************************************************************************
 *  THIS PAGE EXISTS FOR USE WITHIN THE GLOBAL.MIN.JS JQUERY LIBRARY
 *
 *  Reads the Weather conditions from the database and outputs it as a JSON encoded
 *  array to be used within the JQuery library to update the weather display
 * ****************************************************************************/
//Configuration
include('../application/config/global.config.php');

//Determine Database Type, Load Class & Setup Object
switch (DB_TYPE) {
    case DBTYPE_MYSQL:      require_once('../application/classes/mysql.class.php');
        $oDB = new MySQLDatabase(DB_HOST,DB_NAME,DB_USER,DB_PASS);
        break;
    case DBTYPE_SQLITE:     require_once('../application/classes/sqlite.class.php');
        $oDB = new SQLiteDatabase(DB_PATH);
        break;
    default:                die('Unknown Database Type: '.DB_TYPE);
}

//Query the Current Inside Conditions
    $sSQL = 'SELECT timestamp, humidity, temp_c, temp_f FROM inside';
    $aResults = $oDB->query($sSQL);

//Determine what data we're interested in
    if(WEATHER_METRIC) {
        $sTemp = $aResults[0]['temp_c'];
        $sTempType = 'C';
    } else {
        $sTemp = $aResults[0]['temp_f'];
        $sTempType = 'F';
    }

//Done, Echo json values & clear objects
    echo json_encode(   array(  'time' => $aResults[0]['timestamp'],
                                'temp' => round($sTemp,0),
                                'type' => $sTempType,
                                'humidity' => round($aResults[0]['humidity'],0))
                    );
    unset($aResults,$oDB);
?>