<?php
/* *****************************************************************************
 *  THIS PAGE EXISTS FOR USE WITHIN THE GLOBAL.MIN.JS JQUERY LIBRARY
 *
 *  Reads the Weather conditions from the database2 and outputs it as a JSON encoded
 *  array to be used within the JQuery library to update the weather display
 * ****************************************************************************/
//Configuration
    include('../application/config/global.config.php');

//Determine Database Type, Load Class & Setup Object
    switch (DB_TYPE) {
        case DBTYPE_MYSQL:      require_once('../application/classes/mysql.class.php');
                                $oDB = new MySQLDatabase(DB_HOST,DB_NAME,DB_USER,DB_PASS);
                                break;
        case DBTYPE_SQLITE:     require_once('../application/classes/sqlite.class.php');
                                $oDB = new SQLiteDatabase(DB_PATH);
                                break;
        default:                die('Unknown Database Type: '.DB_TYPE);
    }

//Load the Weather
    require_once('../application/classes/weather.class.php');
    $oWeather = new Weather($oDB,WEATHER_APIKEY,WEATHER_CITY,WEATHER_STATE);
    $aWeatherCurr = $oWeather->getCurrent();
    $aWeatherHour = $oWeather->getHourly();
    $aWeatherAstronomy = $oWeather->getAstronomy();

//If SQLite3, change temperature to zero decimals
    if(DB_TYPE == DBTYPE_SQLITE) {
        $aWeatherCurr['temperature'] = round($aWeatherCurr['temperature']);
    }

//Weather Data stored as Imperial(USA). Convert to Metric(World) ?
    if(WEATHER_METRIC) {
        $aWeatherCurr['temperature'] = $oWeather->convertToCelcius($aWeatherCurr['temperature'],0);
        $aWeatherCurr['temp_type'] = 'C';
        $aWeatherCurr['pressure_type'] = 'mBar';
        $aWeatherCurr['wind_type'] = 'kph';
        $aWeatherCurr['wind_speed'] = $oWeather->convertToKPH($aWeatherCurr['wind_speed'],0);
        $aWeatherCurr['wind'] = $aWeatherCurr['wind_speed'] . ' ' . $aWeatherCurr['wind_dir'];
        for($i=0;$i<count($aWeatherHour);$i++) {
            $aWeatherHour[$i]['temp'] = $oWeather->convertToCelcius($aWeatherHour[$i]['temp'],0);
            $aWeatherHour[$i]['temp_feels'] = $oWeather->convertToCelcius($aWeatherHour[$i]['temp_feels'],0);
            $aWeatherHour[$i]['temp_type'] = 'C';
            $aWeatherHour[$i]['wind_type'] = 'kph';
            $aWeatherHour[$i]['wind_speed'] = $oWeather->convertToKPH($aWeatherHour[$i]['wind_speed'],0);
            $aWeatherHour[$i]['wind'] = $aWeatherHour[$i]['wind_speed']. ' ' . $aWeatherHour[$i]['wind_dir'];
        }
    } else {
        $aWeatherCurr['pressure'] = number_format($aWeatherCurr['pressure'] * 0.02953,2);
        $aWeatherCurr['pressure_type'] = 'inHg';
        $aWeatherCurr['wind_speed'] = number_format($aWeatherCurr['wind_speed'],0);
    }

//Our Output Array
    $aJSON = array(
        'current' => $aWeatherCurr,
        'hourly' => $aWeatherHour,
        'astronomy' => $aWeatherAstronomy,
    );

//Done...
    unset($oDB,$oWeather);
    echo json_encode($aJSON);
    unset($aJSON);
?>