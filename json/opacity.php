<?php
/* *****************************************************************************
 *  THIS PAGE EXISTS FOR USE WITHIN THE GLOBAL.MIN.JS JQUERY LIBRARY
 *
 *  Takes the configured PHP Flux array and outputs it as JSON to
 *  be read through jQuery and toggle the "Flux" Div to adjust
 *  the overall opacity of the page.
 * ****************************************************************************/
    include('../application/config/global.config.php');
    if(!OPACITY_LOCALONLY || $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
        echo json_encode($Opacity);
    } else {
        echo json_encode(array( 0 => 100, 1 => 100, 2 => 100, 3 => 100, 4 => 100,
                                5 => 100, 6 => 100, 7 => 100, 8 => 100, 9 => 100,
                               10 => 100, 11 => 100, 12 => 100, 13 => 100, 14 => 100,
                               15 => 100, 16 => 100, 17 => 100, 18 => 100, 19 => 100,
                               20 => 100, 21 => 100, 22 => 100, 23 => 100)
        );
    }
?>