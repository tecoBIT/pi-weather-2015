<?php
/* *****************************************************************************
 *  THIS PAGE EXISTS FOR USE WITHIN THE GLOBAL.MIN.JS JQUERY LIBRARY
 *
 *  Reads the Forecast from the database2 and outputs it as a JSON encoded
 *  array to be used within the JQuery library to update the weather display
 * ****************************************************************************/
//Configuration
    include('../application/config/global.config.php');

//Determine Database Type, Load Class & Setup Object
    switch (DB_TYPE) {
        case DBTYPE_MYSQL:      require_once('../application/classes/mysql.class.php');
                                $oDB = new MySQLDatabase(DB_HOST,DB_NAME,DB_USER,DB_PASS);
                                break;
        case DBTYPE_SQLITE:     require_once('../application/classes/sqlite.class.php');
                                $oDB = new SQLiteDatabase(DB_PATH);
                                break;
        default:                die('Unknown Database Type: '.DB_TYPE);
    }

//Load the Weather
    require_once('../application/classes/weather.class.php');
    $oWeather = new Weather($oDB,WEATHER_APIKEY,WEATHER_CITY,WEATHER_STATE);
    $aWeatherFore = $oWeather->getForecast();

//Weather Data stored as Imperial(USA). Convert to Metric(World) ?
if(WEATHER_METRIC) {
    for($i=0;$i<count($aWeatherFore);$i++) {
        $aWeatherFore[$i]['temp_low'] = $oWeather->convertToCelcius($aWeatherFore[$i]['temp_low'],0);
        $aWeatherFore[$i]['temp_high'] = $oWeather->convertToCelcius($aWeatherFore[$i]['temp_high'],0);
        $aWeatherFore[$i]['temp_type'] = 'C';
    }
}

//Done...
    unset($oDB,$oWeather);
    echo json_encode($aWeatherFore);
    unset($aWeatherFore);
?>