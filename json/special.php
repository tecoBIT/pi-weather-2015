<?php
/* *****************************************************************************
*  THIS PAGE EXISTS FOR USE WITHIN THE GLOBAL.MIN.JS JQUERY LIBRARY
*
*  Reads the Forecast from the database2 and outputs it as a JSON encoded
*  array to be used within the JQuery library to update the weather display
* ****************************************************************************/
//Configuration
    include('../application/config/global.config.php');

//Is today a special date?
    if(array_key_exists(date('md'),$SpecialDays)) {
        echo json_encode(array('special'=>true,'note'=>$SpecialDays[date('md')]));
    } else {
        echo json_encode(array('special'=>false,'note'=>''));
    }
?>
