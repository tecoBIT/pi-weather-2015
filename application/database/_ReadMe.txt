PERMISSIONS
There are a very specific set of permissions that need to be set if you are using
the SQLite3 database.  The following commands assume a Debian/Raspbian OS and
require you to be accessing the terminal


cd /var/www/weatherpi/application
sudo chgrp www-data database
sudo chgrp www-data database/weather.db
sudo chmod g+w database
sudo chmod g+w database/weather.db


If the above does not reoslve the issues, try the following
    sudo chmod 775 /var/www/weatherpi/application/database
    sudo chmod 666 /var/www/weatherpi/application/database/weather.db

