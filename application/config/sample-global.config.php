<?php
//******************************************************************************
//ONCE CONFIGURED, SAVE THIS AS "global.config.php"
//******************************************************************************

//Timezone
//  Set this to your timezone.  Complete list available here:
//  http://php.net/manual/en/timezones.america.php
    date_default_timezone_set('America\New_York');

//Webserver Domain/IP Configuration (NO TRAILING SLASH)
//  The Address or IP Address that this page is running on
//  Must include HTTP or HTTPS but MUST NOT have a trailing slash
//  Example:  http://192.168.0.10
    define('SITE_HOME',             'http://127.0.0.1');

//Static assignments for our database types.
//DO NOT CHANGE these values. Instead uncomment the DB_TYPEs below
    define('DBTYPE_MYSQL',          1);
    define('DBTYPE_SQLITE',         2);

//Sensitive MYSQL Database Settings (mysql.class)
//  If you want to use MySQL, UNCOMMENT these settings
//  and then comment out the SQLITE3 settings.
    //define('DB_TYPE',               DBTYPE_MYSQL);
    //define('DB_HOST',               'localhost');
    //define('DB_NAME',               'TableName');
    //define('DB_USER',               'username');
    //define('DB_PASS',               'p@55w0rd');

//Sensitive SQLITE3 Database Settings (sqlite.class)
//  If you do not want to use SQLITE, comment out these settings
//  and instead UNCOMMENT the MySQL Class settings
    define('DB_TYPE',               DBTYPE_SQLITE);
    define('DB_PATH',               '/var/www/application/database/weather.db');

//Sensitive Settings for Weather.Class
//  Your Weather Underground API Key and City/State to look up
//  Sign up for a Weather Underground FREE developer API Key below
//  http://www.wunderground.com/weather/api/d/login.html
    define('WEATHER_APIKEY',        '0000000000000000');
    define('WEATHER_STATE',         'TX');
    define('WEATHER_CITY',          'Dallas');

//Weather Display
//  If TRUE, display as Metric.     Temperatures are in celsius and pressure in mBar
//  If FALSE, display as Imperial.  Temperatures are in Fahrenheit and pressure inHg
    define('WEATHER_METRIC',        false);

//Weather History
//  Define the number of days that should be stored for weather history
    define('WEATHER_HISTORYDAYS',     3);

//Fade out Page (Hour => Opacity (100 = Full, 0 = Invisible)
//  A pseudo-brightness class that handles opacity values on an hourly basis
//  The array is configured as "Hour => Opacity" where hour is military time
//  and opacity a value between 0 and 100.  0 is Completely black, 100 = complete visibility
    $Opacity = array(
        0 => 25,    1 => 25,    2 => 10,    3 => 10,    4 => 10,    5 => 10,
        6 => 30,    7 => 50,    8 => 50,    9 => 50,    10 => 60,   11 => 70,
        12 => 80,   13 => 80,   14 => 80,   15 => 80,   16 => 80,   17 => 80,
        18 => 80,   19 => 80,   20 => 80,   21 => 70,    22 => 60,   23 => 50
    );

//Should the Opacity/Fade Out only be used when requests are local?
//  This allows you to set the opacity of the output for your kiosk while allowing
//  remote calls to maintain full opacity.  General idea is that if your Pi is hooked
//  up to a 24/7 display you may want to adjust brightness, but it you were to access
//  the weather from your desktop or phone, you'd want regular 100% brightness to be applied.
//      If True:    Opacity values are only used when browser IP is 127.0.0.1
//      If False:   Opacity is always set regardless of who requested the page
    define('OPACITY_LOCALONLY',     true);

//Special Days Configuration (Fade messages in on special dates)
//  Here you can configure custom messages that fade in/out with the date display
//  on pre-defined special days.  The format is "KEY" => "Message" where the key
//  is a Day + Month, padded to two digits each.  For example, August 1st is 0801
//  while December 31st is 1231.  Each Entry should be as follows '0801' => 'Hello World',
//  NOTE: The last entry in your Array SHOULD NOT have a trailing comma. Every other item should.
//        Use \' if you want to insert an apostrophe, every other character is native
    $SpecialDays = array(
        '0101' => 'Happy New Year',
        '0214' => 'Happy Valentine\'s Day',
        '0308' => 'International Women\'s Day', '0314' => 'Happy Pi Day', '0317' => 'Happy St. Patrick\'s Day',
        '0401' => 'It\' April Fool\'s Day',
        '0504' => 'It\'s Cinco de Mayo',
        '0701' => 'Happy Canada Day', '0704' => 'American Independence Day!',
        '1009' => 'Happy Leif Erikson Day', '1031' => 'Happy Halloween',
        '1111' => 'Remembrance Day', '1119' => 'International Men\'s Day',
        '1223' => 'Festivus for the Rest of us', '1224' => 'Christmas Eve', '1225' => 'Merry Christmas', '1231' => 'New Year\'s Eve'
    );
?>