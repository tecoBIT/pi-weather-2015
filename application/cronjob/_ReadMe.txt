Schedule a Crontab Job to update weather hourly or upon any schedule you deem
appropriate.  This should be done as root to avoid read-write errors with the
database while apache has it in use.

From the terminal:
    sudo crontab -e

Add the following line to update everytime it is minute zero of any hour on any day
Minute - Hour - Day of Month - Month - Day of Week - Command
    0 * * * * php /var/www/application/cronjob/cron.php

Close & Save the Document
    CTRL+X
    ENTER
    Y
    ENTER