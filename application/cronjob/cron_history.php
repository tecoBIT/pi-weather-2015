<?php
/* *****************************************************************************
 *  THIS PAGE IS WHAT UPDATES THE WEATHER HISTORY
 *
 *  You should have this page triggered automatically through a server-side
 *  cron job on an hourly basis.  To Do So:
 *
 *      1:  Log into your Linux Desktop & open a terminal
 *          or Log into your Linux SSH Terminal
 *      2:  Type the following command:
 *              sudo crontab -e
 *      3:  If prompted, provide your password
 *      4:  Scroll down to the bottom and add the following line
 *              0 * * * * php /var/www/piweather/cronjob/cron_history.php
 *      5:  Ctrl+X to close, save when prompted
 *      6:  Now this script will run every hour at the top of the hour (minute: 0)
 * ****************************************************************************/

//Load Configuration
$sAppPath = dirname(dirname(__FILE__));
include($sAppPath.'/config/global.config.php');

//Determine Database Type, Load Class & Setup Object
switch (DB_TYPE) {
    case DBTYPE_MYSQL:      require_once($sAppPath.'/classes/mysql.class.php');
        $oDB = new MySQLDatabase(DB_HOST,DB_NAME,DB_USER,DB_PASS);
        break;
    case DBTYPE_SQLITE:     require_once($sAppPath.'/classes/sqlite.class.php');
        $oDB = new SQLiteDatabase(DB_PATH);
        break;
    default:                die('Unknown Database Type: '.DB_TYPE);
}

//Load Outside Weather Conditions **************************************************************************************
    //Get Current Weather Conditions
    require_once($sAppPath.'/classes/weather.class.php');
    $oWeather = new Weather($oDB,WEATHER_APIKEY,WEATHER_CITY,WEATHER_STATE);
    $aOutside = $oWeather->getCurrent();
    unset($oWeather);


//Load Inside Weather Conditions ***************************************************************************************
//We save everything in this database in the Imperial system.  We'll use logic on the output page to convert to metric
    //Query the Database
    $sSQL = 'SELECT humidity, temp_f FROM inside';
    $aInside = $oDB->query($sSQL);

    //Prepare the History Update
    $sSQL = 'INSERT INTO history' .
            '(datestamp, conditions, pressure, tempf_inside, tempf_outside, humid_inside, humid_outside) ' .
            'VALUES(:time,:conditions,:pressure,:tempin,:tempout,:humidin,:humidout)';
    $aParams = array(
        'time' => date('U'),
        'conditions' => $aOutside['conditions'],
        'pressure' => number_format($aOutside['pressure']*0.02953,2),                                                   //Convert to inHg (this is personal use)
        'tempin' => number_format($aInside[0]['temp_f'],1),
        'tempout' => $aOutside['temperature'],
        'humidin' => $aInside[0]['humidity'],
        'humidout' => $aOutside['humidity']
    );
    $oDB->execute($sSQL,$aParams);

//Delete Outdated Records **********************************************************************************************
// Works on the assumption that you're recording history every hour, thus we take the maximum amount of days set in
// the global config and multiply it by 24. Delete any records that are older than this
    $aParams = array('max' => date('U') - (WEATHER_HISTORYDAYS * 24 * 60 * 60));
    $sSQL = 'DELETE FROM history WHERE datestamp < :max';
    $oDB->execute($sSQL,$aParams);

//Done
unset($oDB);
?>