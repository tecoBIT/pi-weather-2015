<?php
/* *****************************************************************************
 *  THIS PAGE IS WHAT UPDATES THE WEATHER DATABASE FROM THE WUNDERGROUND API
 *
 *  You should have this page triggered automatically through a server-side
 *  cron job on an hourly basis.  To Do So:
 *
 *      1:  Log into your Linux Desktop & open a terminal
 *          or Log into your Linux SSH Terminal
 *      2:  Type the following command:
 *              sudo crontab -e
 *      3:  If prompted, provide your password
 *      4:  Scroll down to the bottom and add the following line
 *              0 * * * * php /var/www/piweather/cronjob/cron.php
 *      5:  Ctrl+X to close, save when prompted
 *      6:  Now this script will run every hour at the top of the hour (minute: 0)
 * ****************************************************************************/

    //Load Configuration
        $sAppPath = dirname(dirname(__FILE__));
        include($sAppPath.'/config/global.config.php');

    //Determine Database Type, Load Class & Setup Object
        switch (DB_TYPE) {
            case DBTYPE_MYSQL:      require_once($sAppPath.'/classes/mysql.class.php');
                                    $oDB = new MySQLDatabase(DB_HOST,DB_NAME,DB_USER,DB_PASS);
                                    break;
            case DBTYPE_SQLITE:     require_once($sAppPath.'/classes/sqlite.class.php');
                                    $oDB = new SQLiteDatabase(DB_PATH);
                                    break;
            default:                die('Unknown Database Type: '.DB_TYPE);
        }

    //Load Weather Class & Create Object
        require_once($sAppPath.'/classes/weather.class.php');
        $oWeather = new Weather($oDB,WEATHER_APIKEY,WEATHER_CITY,WEATHER_STATE);

    //Update Weather
        $oWeather->update();

    //Done
        unset($oWeather,$oDB);
?>