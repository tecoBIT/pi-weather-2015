<?php
//WEATHER.CLASS ----------------------------------------------------------------
/*      @author         Teco <teco34@gmail.com>
 *      @created        December 6, 2012
 *      @refactored     March 10, 2015 (Streamlined for Pi-Weather)
 *      @dependency     mysql.class.php
 *      @spacing        spaces (tab = 4 spaces)
 */
// NOTES -----------------------------------------------------------------------
/*  You must provide an API Key to the weather underground service for this class
 *  to be functional.  Edit this in the Weather constants as WEATHER_APIKEY
 */
// FUNCTION LIST ---------------------------------------------------------------
/*      getAstronomy()      Public      Gets the current sunrise/sunset values
 *      getCurrent()        Public      Gets the Current Weather Conditions
 *      getForecast()       Public      Gets the Cached Weather Forecast
 *      getHourly()         Public      Gets the cached Weather Forecast in hourly chunks
 *      getWeatherData()    Private     Requests XML weather data from Weather Underground
 *      isCacheOld()        Public      Checks to see if the weather cache is old.
 *      relativeTime()      Private     Converts an Epoch timestamp to relative time (1 day ago, etc)
 *      setup()             Private     Creates database2 tables (called from __construct())
 *      update()            Public      Updates the local cached weather information
 */

//******************************************************************************
if (basename($_SERVER['REQUEST_URI']) == basename(__FILE__)) { exit();}
//******************************************************************************

class Weather
{
    //Database Settings
    const TABLE_NOW = 'outside';                                                //EDIT: Table name for current weather
    const TABLE_FORE = 'forecast';                                              //EDIT: Table name for weather forecasts
    const TABLE_HOUR = 'hourly';                                                //EDIT: Table name for the hourly weather
    const TABLE_ASTRO = 'astronomy';                                            //EDIT: Table name for the astronomy information
    //Time Constants
    const A_MINUTE = 60;
    const A_HOUR = 3600;
    const A_DAY = 86400;
    //Weather Data Configurations
    var $WEATHER_APIKEY;                                                        //Set in Construct()
    var $WEATHER_STATE;                                                         //Set in Construct()
    var $WEATHER_CITY;                                                          //Set in Construct()
    //Remote Weather Addresses
    const WEATHER_URLNOW = 'http://api.wunderground.com/api/%APIKEY%/conditions/q/%STATE%/%CITY%.json';
    const WEATHER_URLFORE = 'http://api.wunderground.com/api/%APIKEY%/forecast/q/%STATE%/%CITY%.json';
    const WEATHER_URLHOUR = 'http://api.wunderground.com/api/%APIKEY%/hourly/q/%STATE%/%CITY%.json';
    const WEATHER_URLASTRO = 'http://api.wunderground.com/api/%APIKEY%/astronomy/q/%STATE%/%CITY%.json';
    //Connectivity
    var $oDB;                                                                   //mysql.class.php object

//PUBLIC FUNCTIONS =============================================================
    public function __construct($oDB, $APIKey, $City, $State, $bRunSetup = false) {
        //Database Connectivity
        $this->oDB = $oDB;
        //Weather Settings from Global.config
        $this->WEATHER_APIKEY = ($APIKey == '') ? '00000000' : $APIKey;
        $this->WEATHER_CITY = ($City == '') ? 'dallas' : $City;
        $this->WEATHER_STATE = ($State == '') ? 'TX' : $State;
        //Run Setup?
        if ($bRunSetup) {
            self::setup();
        }
    }

    //[c]onvertToCelcius
    /*  Converts an inbound Fahrenheit temperature to Celcius with a
        configurable amount of accuracy (Decimal Places)
        @params     integer     required        The Fahrenheit Temperature to convert
        @params     integer     optional        The number of decimal place to return
        @returns    float                       The Temperature converted to celcius
                                                e.g.: convert(72,2) = 22.22  && convert(72,0) = 22 */
    public function convertToCelcius($Temp,$Accuracy=0) {
        return number_format((($Temp-32)/1.8),$Accuracy);
    }

    //[c]onvertToKPH
    /*  Converts a "Miles Per Hour" reading to "Kilometers Per Hour"
        with a configurable amount of accuracy (Decimal Places)
        @params     integer     required        The MPH Input
        @params     integer     optional        The Number of decimal places to return
        @returns    float                       The Speed converted to KPH from MPH)
                                                e.g: convert(22,2) = 35.40 && convert(22,0) = 35) */
    public function convertToKPH($Input,$Accuracy=0) {
        return number_format(($Input*1.6),$Accuracy);
    }

    //[g]etAstronomy
    /*  Gets the current sunrise/sunset times
        @params     none
        @returns    array       array('sunrise'=>(string), 'sunset'=>(string)); */
    public function getAstronomy() {
        $sSQL = 'SELECT sunriseHour, sunriseMinute, sunsetHour, sunsetMinute FROM ' . self::TABLE_ASTRO;
        $aResults = $this->oDB->query($sSQL,array());
        return $aResults[0];
    }

    //[g]etCurrent
    /*  Gets the current weather conditions from the database2 cache
        @params     none
        @returns    array       array('temperature'=>(float), 'conditions'=>(string), 'humidity'=>(integer),
                                      'wind'=>(string), 'icon'=>(string),'timestamp'=>(integer),
                                      'relativetime'=>(string), 'day'=>(integer), 'month'=>(string),
                                      'year'=>(integer), 'dayname'=>(string) */
    public function getCurrent() {
        //Query Current weather conditions
        $sSQL = 'SELECT temperature, conditions, humidity, wind, pressure, timestamp, icon FROM ' . self::TABLE_NOW;
        $aResults = $this->oDB->query($sSQL, array());
        //Split up the Wind
        $aTemp = explode(' ',$aResults[0]['wind']);

        //Return the Record
        return array('timestamp' => $aResults[0]['timestamp'],
                     'relativetime' => self::relativeTime($aResults[0]['timestamp']),
                     'dayname' => date('l', $aResults[0]['timestamp']),
                     'day' => date('j', $aResults[0]['timestamp']),
                     'month' => date('F', $aResults[0]['timestamp']),
                     'year' => date('Y', $aResults[0]['timestamp']),
                     'temperature' => $aResults[0]['temperature'],
                     'temp_type' => 'F',
                     'conditions' => $aResults[0]['conditions'],
                     'humidity' => $aResults[0]['humidity'],
                     'wind' => $aResults[0]['wind'],
                     'wind_speed' => $aTemp[0],
                     'wind_dir' => $aTemp[2],
                     'wind_type' => 'mph',
                     'pressure' => $aResults[0]['pressure'],
                     'icon' => $aResults[0]['icon']);
    }

    //[g]etForecast
    /*  Gets and returns a formatted array of the 3 day weather forecast
        @params     none
        @returns    array       array(	array('isToday'=>(Boolean), 'day'=>(integer), 'month'=>(integer),
                                              'year'=>(integer), 'dayname'=>(string), 'conditions'=>(string),
                                              'temp_low'=>(float), 'temp_high'=>(float), 'precipitation'=>(integer),
                                              array(....), array(...), (etc) */
    public function getForecast() {
        $aReturn = array();
        $sSQL = 'SELECT day, conditions, temp_low, temp_high, pop, icon FROM ' .
            self::TABLE_FORE . ' ORDER BY day ASC';
        $aResults = $this->oDB->query($sSQL, array());
        //Loop through and reformat
        foreach ($aResults AS $Result) {
            $aReturn[] = array( 'isToday' => (date('N') == date('N', $Result['day'])) ? true : false,
                                'day' => date('j', $Result['day']),
                                'month' => date('F', $Result['day']),
                                'year' => date('Y', $Result['day']),
                                'dayname' => date('l', $Result['day']),
                                'conditions' => $Result['conditions'],
                                'temp_low' => $Result['temp_low'],
                                'temp_high' => $Result['temp_high'],
                                'temp_type' => 'F',
                                'precipitation' => $Result['pop'],
                                'icon' => $Result['icon']);
        }
        return $aReturn;
    }

    //[g]etHourly
    /*  Gets the weather forecast, by hour, for the next 36 hours
        @params     none
        @returns    array       array(	array(	'isToday'=>(Boolean), 'dayname'=>(string), 'hour'=>(integer)
                                                'epoch' => (integer), 'icon'=>(string), 'conditions'=>(string),
                                                'temp' => (integer), 'temp_feels' => (integer), 'wind'=>(string)
                                                'precipitation'=>(integer)),
                                                array(...), array(...), etc...); */
    public function getHourly() {
        $aReturn = array();
        $sSQL = 'SELECT hour, icon, conditions, temperature, temperature_feels, ' .
                'wind, precipitation FROM ' . self::TABLE_HOUR . ' ORDER BY hour ASC LIMIT 0,24';
        $aResults = $this->oDB->query($sSQL, array());
        //Loop through and reformat
        foreach ($aResults AS $Result) {
            $aTemp = explode(' ',$Result['wind']);
            $aReturn[] = array( 'isToday' => (date('N') == date('N', $Result['hour'])) ? true : false,
                                'dayname' => date('l', $Result['hour']),
                                'hour' => date('h:i A', $Result['hour']),
                                'epoch' => $Result['hour'],
                                'icon' => $Result['icon'],
                                'conditions' => $Result['conditions'],
                                'temp' => $Result['temperature'],
                                'temp_feels' => $Result['temperature_feels'],
                                'temp_type' => 'F',
                                'wind' => $Result['wind'],
                                'wind_speed' => $aTemp[0],
                                'wind_dir' => $aTemp[1],
                                'wind_type' => 'mph',
                                'precipitation' => $Result['precipitation']);
        }
        return $aReturn;
    }

    //[u]pdate
    /*  Updates the local weather cache with contents from the Weather underground API
        @params     none
        @returns    array                       array((bool)current_updated, (bool)forecast_updated,
                                                      (bool)hourly_updated, (bool)astronomy_updated,
                                                      (string)reason); */
    public function update() {
        $bCurrent = false;
        $bForecast = false;
        $bHourly = false;
        $bAstro = false;
        //Get the Current Weather conditions -----------------------------------
        $aWeather = json_decode(self::getWeatherData(self::WEATHER_URLNOW), true);
        if ($aWeather) {
            //Delete Old Records (if any)
            $sSQL = 'DELETE FROM ' . self::TABLE_NOW;
            $this->oDB->execute($sSQL, array());
            //Insert Current Weather
            $sSQL = 'INSERT INTO ' . self::TABLE_NOW .
                    ' (temperature, conditions, humidity, wind, pressure, timestamp, icon) ' .
                    'VALUES(:temp,:cond,:humid,:wind,:pressure,:time,:icon)';
            $aParams = array(   ':temp' => (float)$aWeather['current_observation']['temp_f'],
                                ':cond' => (string)$aWeather['current_observation']['weather'],
                                ':humid' => (integer)$aWeather['current_observation']['relative_humidity'],
                                ':wind' => (string)$aWeather['current_observation']['wind_mph'] . '  ' .
                                           (string)$aWeather['current_observation']['wind_dir'],
                                ':pressure' => (integer)$aWeather['current_observation']['pressure_mb'],
                                ':time' => date('U'),
                                ':icon' => (string)$aWeather['current_observation']['icon']
            );
            $this->oDB->execute($sSQL, $aParams);
            $bCurrent = true;
        }
        //Get the Forecast Weather conditions ----------------------------------
        $aWeather = json_decode(self::getWeatherData(self::WEATHER_URLFORE), true);
        if ($aWeather) {
            //Clear the Current Forecast Records
            $sSQL = 'DELETE FROM ' . self::TABLE_FORE;
            $this->oDB->execute($sSQL, array());
            //Insert Statement for Subsequent Days
            $sSQL = 'INSERT INTO ' . self::TABLE_FORE . '(day,conditions,temp_low,temp_high,pop,icon)' .
                    ' VALUES(:day,:cond,:temp_low,:temp_high,:pop,:icon)';
            //Loop through each Day's forecast
            foreach ($aWeather['forecast']['simpleforecast']['forecastday'] AS $Day) {
                $aParams = array(':day' => (integer)$Day['date']['epoch'],
                                 ':cond' => (string)$Day['conditions'],
                                 ':temp_low' => (float)$Day['low']['fahrenheit'],
                                 ':temp_high' => (float)$Day['high']['fahrenheit'],
                                 ':pop' => (integer)$Day['pop'],
                                 ':icon' => (string)$Day['icon']
                );
                $this->oDB->execute($sSQL, $aParams);
            }
            $bForecast = true;
        }
        //Get the Hourly Conditions --------------------------------------------
        $aWeather = json_decode(self::getWeatherData(self::WEATHER_URLHOUR), true);
        if ($aWeather) {
            //Clear the current hourly records
            $sSQL = 'DELETE FROM ' . self::TABLE_HOUR;
            $this->oDB->execute($sSQL, array());
            //Prepare the insert Statement
            $sSQL = 'INSERT INTO ' . self::TABLE_HOUR . '(hour,icon,conditions,temperature,temperature_feels, ' .
                    'wind, precipitation) VALUES(:epoch,:icon,:conditions,:temp,:temp_feels,:wind,:pop)';
            //Loop through each hour
            foreach ($aWeather['hourly_forecast'] AS $Hour) {
                $aParams = array(':epoch' => $Hour['FCTTIME']['epoch'],
                                 ':icon' => $Hour['icon'],
                                 ':conditions' => $Hour['condition'],
                                 ':temp' => $Hour['temp']['english'],
                                 ':temp_feels' => $Hour['feelslike']['english'],
                                 ':wind' => $Hour['wspd']['english'] . ' ' . $Hour['wdir']['dir'],
                                 ':pop' => $Hour['pop']
                );
                $this->oDB->execute($sSQL, $aParams);
            }
            $bHourly = true;
        }
        //Get the Astronomy Values ---------------------------------------------
        $aWeather = json_decode(self::getWeatherData(self::WEATHER_URLASTRO),true);
        if($aWeather) {
            //Clear the current values
            $sSQL = 'DELETE FROM ' . self::TABLE_ASTRO;
            $this->oDB->execute($sSQL, array());
            //Insert the New Values
            $sSQL = 'INSERT INTO ' . self::TABLE_ASTRO . '(sunriseHour, sunriseMinute, sunsetHour, sunsetMinute) ' .
                    'VALUES(:sunriseHour, :sunriseMinute, :sunsetHour, :sunsetMinute)';
            $aParams = array(':sunriseHour' => $aWeather['sun_phase']['sunrise']['hour'],
                             ':sunriseMinute' => $aWeather['sun_phase']['sunrise']['minute'],
                             ':sunsetHour' => $aWeather['sun_phase']['sunset']['hour'],
                             ':sunsetMinute' => $aWeather['sun_phase']['sunset']['minute']);
            $this->oDB->execute($sSQL, $aParams);
            $bAstro = true;
        }

        //Return values
        return array($bCurrent, $bForecast, $bHourly, $bAstro, 'completed');
    }

//PRIVATE FUNCTIONS ============================================================
    //[g]etWeatherData
    /*  Gets remote data from Weather Underground for either current
        conditions or for the forecast.
        @params     boolean     optional        If True get Current, else get Forecast
        @returns    object                      simple_xml formatted object with weather data */
    private function getWeatherData($sAddress = '') {
        //Determine where we're looking
        if ($sAddress == '') {
            $sURL = self::WEATHER_URLNOW;
        } else {
            $sURL = $sAddress;
        }
        //Fill in variables
        $sURL = str_replace('%APIKEY%', $this->WEATHER_APIKEY, $sURL);
        $sURL = str_replace('%STATE%', $this->WEATHER_STATE, $sURL);
        $sURL = str_replace('%CITY%', $this->WEATHER_CITY, $sURL);
        //Curl the JSON Data
        $oCurl = curl_init($sURL);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($oCurl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, 0);
        //Request the Data
        return curl_exec($oCurl);
    }

    //[r]elativeTime
    /*	Compares a unix epoch against the current time and returns a string
        that describes the relative time difference between the two.
        For example, if today is 11/12/2012 and the function was told to compare
        today against 11/11/2012 it would return 'a day ago'
        @params     integer     required        Unix Epoch timestamp of date in the past
        @returns    string                      Relative time compared to today */
    public function relativeTime($iPast = 0) {
        $iDiff = date('U') - $iPast;
        if ($iDiff < self::A_MINUTE) {
            return 'a moment ago';
        } else {
            if (($iDiff >= self::A_MINUTE) && ($iDiff < self::A_MINUTE * 1.5)) {
                return 'a minute ago';
            } else {
                if (($iDiff >= self::A_MINUTE * 1.5) && ($iDiff < self::A_HOUR)) {
                    return round($iDiff / self::A_MINUTE) . ' minutes ago';
                } else {
                    if (($iDiff >= self::A_HOUR) && ($iDiff < self::A_HOUR * 1.5)) {
                        return 'an hour ago';
                    } else {
                        if (($iDiff >= self::A_HOUR * 1.5) && ($iDiff < self::A_DAY)) {
                            return round($iDiff / self::A_HOUR) . ' hours ago';
                        } else {
                            if (($iDiff >= self::A_DAY) && ($iDiff < self::A_DAY * 1.5)) {
                                return 'a day ago';
                            } else {
                                return round($iDiff / self::A_DAY) . ' days ago';
                            }
                        }
                    }
                }
            }
        }
    }

    //[s]etup
    /*  Creates the necessary weather database2 tables if requested.
        This function can only be called by passing "$blnRunSetup=true" to __construct()
        @params     none
        @returns    none */
    private function setup() {
        //Create the Current Weather Table
        $sSQL = 'CREATE TABLE IF NOT EXISTS ' . self::TABLE_NOW . ' (' .
                'temperature decimal(10,0) NOT NULL,' .
                'conditions varchar(128) COLLATE utf8_unicode_ci NOT NULL,' .
                'humidity decimal(10,0) NOT NULL,' .
                'wind varchar(128) COLLATE utf8_unicode_ci NOT NULL,' .
                'pressure int(11) NOT NULL,' .
                'timestamp int(11) NOT NULL,' .
                'icon varchar(255) COLLATE utf8_unicode_ci NOT NULL' .
                ') ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
        $this->oDB->execute($sSQL, array());
        //Create the Weather Forecast table
        $sSQL = 'CREATE TABLE IF NOT EXISTS ' . self::TABLE_FORE . ' (' .
                'day int(11) NOT NULL,' .
                'conditions varchar(255) COLLATE utf8_unicode_ci NOT NULL,' .
                'temp_low decimal(10,0) NOT NULL,' .
                'temp_high decimal(10,0) NOT NULL,' .
                'pop decimal(10,0) NOT NULL,' .
                'icon varchar(255) COLLATE utf8_unicode_ci NOT NULL' .
                ') ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
        $this->oDB->execute($sSQL, array());
        //Create the Weather Hourly Table
        $sSQL = 'CREATE TABLE IF NOT EXISTS ' . self::TABLE_HOUR . ' (' .
                'hour int(11) NOT NULL, ' .
                'icon varchar(255) COLLATE utf8_unicode_ci NOT NULL, ' .
                'conditions varchar(128) COLLATE utf8_unicode_ci NOT NULL, ' .
                'temperature int(11) NOT NULL, ' .
                'temperature_feels int(11) NOT NULL, ' .
                'wind varchar(128) COLLATE utf8_unicode_ci NOT NULL, ' .
                'precipitation int(11) NOT NULL ' .
                ') ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
        $this->oDB->execute($sSQL, array());
        //Create the Astronomy Table
        $sSQL = 'CREATE TABLE IF NOT EXISTS ' . self::TABLE_ASTRO . ' (' .
                'sunriseHour int(2) NOT NULL, ' .
                'sunriseMinute int(2) NOT NULL, ' .
                'sunsetHour int(2) NOT NULL, ' .
                'sunsetMinute int(2) NOT NULL, ' .
                ') ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
        $this->oDB->execute($sSQL,array());
    }
}