<?php
//MYSQL.CLASS ------------------------------------------------------------------
/*  @author     Teco <teco34@gmail.com>
 *  @created    February 18, 2015
 *  @spacing    spaces (tab = 4 spaces)
 */
// FUNCTION LIST ---------------------------------------------------------------
/*  __construct     public      Creates a database2 connection, auto-called on object creation
 *  __destruct      public      Disconnects from database2. Auto-called on object deletion
 *  disconnect      public      Alias for __destruct for optional manual disconnection
 *  execute         public      Alias for Query :: Runs a query, returns a row count of affected records
 *  query           public      Queries the database2, returns array sets of records
 */

//******************************************************************************
if (basename($_SERVER['REQUEST_URI']) == basename(__FILE__)) { exit(); }
//******************************************************************************

class MySQLDatabase {
    private $oDB = null;                                                        //The Database Connection

    //[_]construct
    /*  Creates a new MySQL Connection that can be used repeatedly
        @params     string      required        The Server Host (e.g.:  mysql.server.com)
        @params     string      required        The Database (e.g.: mydatabase)
        @params     string      required        Database Username (e.g.: MyUser)
        @params     string      required        Database Password (e.g. p@55w0rd)
        @returns    array       if success      array(true, 'connection_established');
                                if failure      array(false, Exception_Details) */
    public function __construct($Host, $Database, $User, $Pass) {
        try {
            $this->oDB = new PDO('mysql:host=' . $Host . ';dbname=' . $Database, $User, $Pass);
            return array(true, 'Connection_Established');
        } catch (Exception $oException) {
            echo '<HR>Database Failed to Connect<HR>';
            return array(false, $oException);
        }
    }

    //[_]destruct (alias disconnect)
    /*  Terminates the database2 connection and clears connection objects from memory
        If class is unset the __destruct is called. disconnect() acts as a manual alias
        @params     none
        @returns    nothing */
    public function disconnect() {
        $this->__destruct();
    }

    public function __destruct() {
        $this->oDB = null;
        foreach ($this AS $key => $value) {
            unset($this->$key);
        }
    }

    //[e]execute
    /*  Alias for the Query function that automatically sets "return results" to
        false to mimic a "SQL Execute" instead of "SQL Query". Semantics.
        @params     string      required        The SQL Query to be executed (e.g.: 'delete from users where ID = :id AND Name = :name)
        @params     array       optional        An array of parameter substitutes (e.g.: Params('id' => 1, 'name' => 'bob'))
        @returns    integer                     The Number of affected rows */
    public function execute($Query, $Params = array()) {
        return self::Query($Query, $Params, false);
    }

    //[q]uery
    /*  The 'meat and potatoes' the runs a query against the database2.   Though the
        function is titled Query, it's Query & Execute all in one.  Only difference
        being that the execute alias forces a return of affected rows (count) while Query
        optionally can return affected row count OR all the records.
        @params     string      required        The SQL Query to be executed (e.g.: 'select * from users where ID = :id OR Name = :names)
        @params     array       optional        An array of parameter substitutes (e.g.: Params('id' => 1, 'name' => 'bob'))
        @params     boolean     optional        Return Results? (if yes, return records, if no, return count of affected records)
        @returns    variant                     If Return Results = True: array( row1(...), row2(...), row3(...), etc)
                                                If Return Results = False : integer (Number of affected rows) */
    public function query($Query, $Params, $ReturnResults = true) {
        if ($Query == '' || $Query == null) {
            return array(false, 'querynull');
        }
        $oPrepared = $this->oDB->prepare($Query);
        $oPrepared->execute($Params);
        if ($ReturnResults) {
            return $oPrepared->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return $oPrepared->rowCount();
        }
        unset($oPrepared);
    }
}
?>