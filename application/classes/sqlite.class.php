<?php
//SQLITE.CLASS -----------------------------------------------------------------
/*  @author     Teco <teco34@gmail.com>
 *  @created    March 16, 2015
 *  @spacing    spaces (tab = 4 spaces)
 */
// FUNCTION LIST ---------------------------------------------------------------
/*  __construct     public      Makes note of the Database Path
 *  __destruct      public      Disconnects from database. Auto-called on object deletion
 *  disconnect      public      Alias for __destruct for optional manual disconnection
 *  execute         public      Alias for Query :: Runs a query, returns a row count of affected records
 *  init            private     Establishes a database connection. nested use from query/execute
 *  getBindType     private     Looks up the variable type and returns an SQLITE_ reference for parameterized queries
 *  query           public      Queries the database2, returns array sets of records
 */

//******************************************************************************
if (basename($_SERVER['REQUEST_URI']) == basename(__FILE__)) { exit(); }
//******************************************************************************
class SQLiteDatabase{
    private $oDB = null;                                                        //Store the connection for local use
    var $DB_PATH;                                                               //Database Path setin in __construct()

//PUBLIC FUNCTIONS -------------------------------------------------------------

    //[_]_construct
    /*  Initializes the object & sets up the path to the database.
        @params     string      required        The path to the Database (Absolute: /var/www/site/database.db)
        @returns    nothing */
    public function __construct($PathToDatabase) {
        $this->DB_PATH = $PathToDatabase;
    }

    //[_]destruct (alias disconnect)
    /*  Terminates the database2 connection and clears connection objects from memory
        If class is unset the __destruct is called. disconnect() acts as a manual alias
        @params     none
        @returns    nothing */
    public function disconnect() {
        $this->__destruct();
    }
    public function __destruct() {
        $this->oDB = null;
        foreach ($this AS $key => $value) {
            unset($this->$key);
        }
    }

    //[e]xecute
    /*  Runs an "EXECUTE" query against the database.  Supports inline query as
        well as parameterized queries.  Returns SQLite3 Object
        @params     string      required        the SQL Query to execute (can be parameterized)
                                                Example: "delete from table where ID = 1"
                                                Example: "delete from table where ID = :id"
        @params     array       optional        If parameterized SQL, an array of params.
                                                Example:   array(':id' => 1);
        @returns    object                      SQLIte3 Result Object */
    public function execute($Query,$Params=array()) {
        self::init();
        if($Params == array()) {
            return $this->oDB->exec($Query);
        } else {
            $oStatement = $this->oDB->prepare($Query);
            foreach($Params AS $Key=>$Value) {
                $oStatement->bindValue($Key,$Value,self::getBindType($Value));
            }
            return $oStatement->execute();
        }
    }

    //[q]uery
    /*  Runs a query against the database and returns the results as an array.
        Supports inline query as well as parameterized queries.
        @params     string      required        the SQL Query to run
                                                Example: "select * from table where ID = 1"
                                                Example: "select * from table where ID = :id"
        @params     array       optional        The parameters for your PDO Query
                                                Example: array(':id' => 1);
        @returns    array                       An Array of rows.
                                                Example:    array(  array('name'=>user1), array('name2'=>user2)); */
    public function query($Query,$Params=array()) {
        self::init();
        //Paramaterized vs Non-Parameterized queries
            if($Params == array()) {
                $oResult = $this->oDB->Query($Query);
            } else {
                $oStatement = $this->oDB->prepare($Query);
                foreach($Params AS $Key=>$Value) {
                    $oStatement->bindValue($Key,$Value,self::getBindType($Value));
                }
                $oResult = $oStatement->execute();
            }
        //Reformat the SQLite object into Arrays
            $aResults = array();
            while($res = $oResult->fetchArray(SQLITE3_ASSOC)) {
                $aResults[] = $res;
            }
            return $aResults;
    }



//PRIVATE FUNCTIONS ------------------------------------------------------------
    //[i]nit
    /*  Initializes the database connect to the specified path.
        This is a private function that is called internally before a query
        is executed.
        @params     none                        Uses DB_PATH global
        @returns    nothing                     Updates the oDB Global */
    private function init() {
        $this->oDB = new SQLite3($this->DB_PATH);
    }

    //[g]etBindType
    /*  Parameterized queries with SQLite require that they not only have a
        key value pair, but they must also specify their Type (e.g.: Integer, string, etc)
        To keep this class compatible with the more widely used MySQL Class, this class
        will automatically lookup and specify the types for the parameterized queries.
        @params     variant     required        The Variable we are going to be using
        @returns    reference                   The SQLITE_ association to use in the query */
    private function getBindType($arg) {
        switch (gettype($arg)) {
            case 'double':      return SQLITE3_FLOAT;   break;
            case 'integer':     return SQLITE3_INTEGER; break;
            case 'boolean':     return SQLITE3_INTEGER; break;
            case 'NULL':        return SQLITE3_NULL;    break;
            case 'string':      return SQLITE3_TEXT;    break;
            default:            return SQLITE3_TEXT;
        }
    }
}
?>