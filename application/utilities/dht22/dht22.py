#!/usr/bin/env python

'''
DHT22.PY -----------------------------------------------------------------------
    @author         Teco <teco34@gmail.com>
    @created        March 27, 2015
    @updated        March 28, 2015
    @dependancy     Adafruit_DHT
                    https://github.com/adafruit/Adafruit_Python_DHT.git
    @spacing        spaces (tab = 4 spaces)
--------------------------------------------------------------------------------
    This script is expected to be run as a SUDO CRONTAB job on a scheduled basis.

    The primary purpose is to query the DHT22 for current conditions and then
    store them in the database for use within the WeatherPi website.

    View the "CONFIGURATION" section to make adjustments as needed
        sDatabase       required    string      The Absolute path to your SQLite database
        iGPIO           required    integer     The Data pin your DHT22 is connected to (e.g.: 4)
        sInsideTable    required    string      The table name to store your data in
--------------------------------------------------------------------------------
'''

# LIBRARIES --------------------------------------------------------------------
import Adafruit_DHT as dht                                                      #Used to read the DHT22 values
import sqlite3 as sqlite                                                        #Used to store the values
import time                                                                     #Used to generate a timestamp
import sys                                                                      #Error Trapping & Script Termination

# CONFIGURATION ----------------------------------------------------------------
sDatabase = "/var/www/application/database/weather.db";                         #Path to Database (Absolute)
sInsideTable = "inside";                                                        #Table name for Data (Must patch /json/inside.php code)
iGPIO = 4;                                                                      #Data Pin the DHT22 is connected to
iSleep = 25;                                                                    #Number of seconds to delay execution (See Step 0)

# FUNCTIONS --------------------------------------------------------------------
def convertToFahrenheit(iCelsius):
    return ((iCelsius*1.8)+32)

# LOGIC / ROUTINE --------------------------------------------------------------
'''
[0] Sleep
    Sleep the script for "N" seconds to delay execution.  This is to ensure that
    in the event this script triggers at the same time as the PHP weather update
    that there isn't a database locking problem.   Otherwise, if the Py locks the
    database, we'll have invalid external data displayed for the forecast.
'''
print "Sleeping for " + str(iSleep) + " seconds...";
time.sleep(iSleep);

'''
[1] Establish a database connection
    If this fails, there is no point to continue
'''
print "Opening Database...";
try:
    oConn = sqlite.connect(sDatabase);
    oCursor = oConn.cursor();
except:
    print "> Error opening database";
    e = sys.exc_info()[0];
    print ">> %s" % e;
    sys.exit(1)

'''
[2] Query the current conditions from the DHT22
    Data comes back in Celsius so
    we'll convert it to Fahrenheit and save both values to the database
'''
print "Querying DHT22...";
iHumidity,iTempC = dht.read_retry(dht.DHT22,iGPIO);
iTempF = convertToFahrenheit(iTempC);
iEpoch = int(time.time());
print "> Done";

'''
[3] Delete existing "inside" records
    We're not storing history, so always clear before insert.
    If you want to store history, besure to modify /json/inside.php to select
    only the most recent record otherwise the "inside" display will be off
'''
print "Deleting existing records...";
try:
    sSQL = "DELETE FROM " + sInsideTable
    oCursor.execute(sSQL)
    print "> Success!"
except:
    print "> Error!";
    e = sys.exc_info()[0];
    print ">> %s" % e;

'''
[4] Prepare Insert Query
    Insert the current conditions into the database. We are going to use a
    simple "inline query" instead of paramaterized because if you have the
    ability to modify and execute this script then you can already manipulate
    the database to your liking.  It would be pointless to try and stop an
    sql inject from someone able to manipulate the script's source.
'''
print "Updating database...";
sSQL = ("INSERT INTO " + sInsideTable + "(timestamp, temp_c, temp_f, humidity) "
        "VALUES(" + str(iEpoch) + ", " + str(iTempC) + ", " + str(iTempF) + ", " + str(iHumidity) + ")")
try:
    oCursor.execute(sSQL);
    oConn.commit();
    print "> Success!"
except:
    print "> Error!";
    e = sys.exc_info()[0];
    print ">> %s" % e;

'''
[5] Done!
    Disconnect and close
'''
oConn.close()
print "Done"