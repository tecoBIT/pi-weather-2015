THIS SCRIPT REQUIRES THE ADAFruit Python DHT Library.

Installation instructions are as follows:
    git clone https://github.com/adafruit/Adafruit_Python_DHT.git
    cd Adafruit_Python_DHT
    sudo apt-get update
    sudo apt-get install build-essential python-dev
    sudo python setup.py install

Updating should be handled through the Cronttab
    sudo crontab -e

Append the following line to update local temperatures every 5 minutes
    */5 * * * * /usr/bin/python /var/www/application/utilities/dht22/dht22.py