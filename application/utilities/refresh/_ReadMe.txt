The "refresh.sh" script is used to force a screen refresh of your kiosk without
having to restart the machine.  This is useful to force reload new javascript
or stylesheet resources.

REQUIREMENTS:
    - Install XDOTools
        sudo apt-get install xdotool
    - Execute Permissions for the Script
        sudo chmod +x /var/www/application/utilities/refresh/refresh.sh

HOW TO USE:
    1:  SSH into your raspberry pi
    2:  Browse to the script folder
        cd /var/www/application/utilities/refresh
    3:  Execute the Refresh Script
        sudo ./refresh.sh
    4:  Your Chromium kiosk will now reload (Simulates pressing F5)