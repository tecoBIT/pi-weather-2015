The "wifi.sh" script is used to repeatedly test the Wireless connection of the
raspberry pi.  In the event the router goes down, or the DHCP lease expires
the script will attempt to reconnect the wireless.

This should be setup as a Cron job to repeatedly test the connection.

REQUIREMENTS:
    - Execute Permissions for the Script
        sudo chmod +x /var/www/application/utilities/wifimonitor/wifi.sh
    - Edit the WiFi Script and set the proper IP Address to test
        Look at line 18. Use the internal IP of your router/dhcp server

HOW TO USE:
    Updating should be handled through the Cronttab
        sudo crontab -e

    Append the following line to update local temperatures every 5 minutes
        */5 * * * * /usr/bin/bash /var/www/application/utilities/wifimonitor/wifi.sh