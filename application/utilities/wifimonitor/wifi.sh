#!/bin/bash

#This script checks a local network IP Address to ensure that 
#a connection is established. If this fails, we are to assume 
#that the wifi connection has dropped. If that is the case then 
#restart the WiFi connection.
#
#This is a result of an issue with the Raspbian OS where it only 
#establishes a WIFI connection once.  If it drops, it never tries 
#to re-establish unless told to by the user. 
#
#This will run as a Cron job and check the network status every 
#5 minutes. If a connection is unavailable, wifi will restart. This
#should negate instances where the router reboots or updates and drops 
#the raspberry pi's wifi connection until the pi itself is rebooted.

TESTIP=192.168.0.1
ping -c4 ${TESTIP} > /dev/null


if [ $? != 0 ]
then
    echo "WiFi seems down, restarting"
    /sbin/ifdown --force 'wlan0'
    sleep 5
    /sbin/ifup --force 'wlan0'
else
    echo "WiFi appears up"
fi
