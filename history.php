<?php
//Server-Side Necessities
    include_once('application/config/global.config.php');

//Header Overrides
    header('Cache-Control: no-cache, no-store, must-revalidate');               // HTTP 1.1 Cache Disabling
    header('Pragma: no-cache');                                                 // HTTP 1.0  Cache Disabling
    header('Expires: 0');                                                       // Proxies Cache Disabling (Probably unnecessary)

//------------------------------------------------------------------------------
//FUNCTIONS --------------------------------------------------------------------
//------------------------------------------------------------------------------
//[d]rawArrow
/*  Determines what type of arrow should be drawn
    @params     integer     required        the Previous value
    @params     integer     required        the Current Value
    @returns    string                      "up", "down", "nochange" */
function drawArrow($Curr, $Next, $Decimals){
    if (number_format($Next,$Decimals) == number_format($Curr,$Decimals)) { return "nochange";}
    if (number_format($Next,$Decimals) < number_format($Curr,$Decimals)) { return "up";}
    if (number_format($Next,$Decimals) > number_format($Curr,$Decimals)) { return "down"; }
}

//[r]eturnPressure
/*  Converts Pressure to Metric if needed since database is in imperial
    @params     integer     required        inMg pressure value
    @returns    string                      if Metric, converts to mBar and returns value
                                            if Imperial, returns value + " inHg" */
function returnPressure($Pressure) {
    if(WEATHER_METRIC) {
        return number_format($Pressure*33.8639,0,'.',''). " mBar";
    } else {
        return number_format($Pressure,2) . " inHg";
    }
}

//[r]eturnTemperature
/*  Converts temperature to Metric if needed since database is in imperial
    @params     integer     required        Fahrenheit temperature
    @returns    string                      If metric, converts to Celcius and returns + "&deg;C"
                                            If imperial, returns value + "&deg;F"; */
function returnTemperature($Temperature) {
    if(WEATHER_METRIC) {
        return number_format(($Temperature-32)/1.8,0) . '&deg;C';
    } else {
        return number_format($Temperature,0) . '&deg;F';
    }
}

//------------------------------------------------------------------------------
// ROUTINE ---------------------------------------------------------------------
//------------------------------------------------------------------------------
//What Mode are we in? MySQL or SQLite
switch (DB_TYPE) {
    case DBTYPE_MYSQL:      require_once('application/classes/mysql.class.php');
                            $oDB = new MySQLDatabase(DB_HOST,DB_NAME,DB_USER,DB_PASS);
                            break;
    case DBTYPE_SQLITE:     require_once('application/classes/sqlite.class.php');
                            $oDB = new SQLiteDatabase(DB_PATH);
                            break;
    default:                die('Unknown Database Type: '.DB_TYPE);
}

//Query Back the Details
    $sSQL = 'SELECT datestamp, conditions, pressure, tempf_inside, tempf_outside, humid_inside, humid_outside ' .
            'FROM history ORDER BY datestamp DESC ';
    $aResults = $oDB->query($sSQL,array());
    unset($oDB);


?>
<!html>
<html>
    <head>
        <title><?php echo WEATHER_HISTORYDAYS . ' day history for ' . WEATHER_CITY . ', ' . WEATHER_STATE ?></title>
        <meta name="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=0.75" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="author" href="<?php echo SITE_HOME ?>/humans.txt" type="text/plain" />
        <link rel="stylesheet" href="<?php echo SITE_HOME ?>/includes/css/reset.css" />
        <link rel="stylesheet" href="<?php echo SITE_HOME ?>/includes/css/style.css" />
        <script src="<?php echo SITE_HOME ?>/includes/js/zepto.min.js" defer></script>
        <script src="<?php echo SITE_HOME ?>/includes/js/global.min.js" defer></script>
        <meta name="x-timestamp-page" content="<?php echo date('U') ?>" />
    </head>
    <body class="scroll">
        <div id="container" class="history">
            <table>
        <?php
            $lastDay = '';
            for($i=0;$i<count($aResults);$i++) {
                if ($lastDay != date('l',$aResults[$i]['datestamp'])) {
                    $lastDay = date('l',$aResults[$i]['datestamp']); ?>
                    <tr>
                        <td class="header" colspan="7">
                            <em><?php echo $lastDay ?></em>
                            <span><?php echo date('F jS, Y', $aResults[$i]['datestamp']) ?></span>
                        </td>
                    </tr>
                    <tr class="titles">
                        <td>Time</td>
                        <td>Conditions</td>
                        <td>Pressure</td>
                        <td>Outside</td>
                        <td>Humidity</td>
                        <td>Inside</td>
                        <td>Humidity</td>
                    </tr>
                <?php } ?>
                    <tr>
                        <td><?php echo date('H:i',$aResults[$i]['datestamp']) ?></td>
                        <td><?php echo $aResults[$i]['conditions'] ?></td>
                        <td><?php echo returnPressure($aResults[$i]['pressure']) ?> <div class="drawsmallarrow <?php echo drawArrow($aResults[$i]['pressure'],$aResults[$i+1]['pressure'],2); ?>"></div></td>
                        <td><?php echo returnTemperature($aResults[$i]['tempf_outside']) ?> <div class="drawsmallarrow <?php echo drawArrow($aResults[$i]['tempf_outside'],$aResults[$i+1]['tempf_outside'],0); ?>"></div></td>
                        <td><?php echo number_format($aResults[$i]['humid_outside'],0) ?>% <div class="drawsmallarrow <?php echo drawArrow($aResults[$i]['humid_outside'],$aResults[$i+1]['humid_outside'],0); ?>"></div></td>
                        <td><?php echo returnTemperature($aResults[$i]['tempf_inside']) ?> <div class="drawsmallarrow <?php echo drawArrow($aResults[$i]['tempf_inside'], $aResults[$i+1]['tempf_inside'],0); ?>"></div></td>
                        <td><?php echo number_format($aResults[$i]['humid_inside'],0) ?>% <div class="drawsmallarrow <?php echo drawArrow($aResults[$i]['humid_inside'], $aResults[$i+1]['humid_inside'],0); ?>"></div></td>
                    </tr>
            <?php  } ?>
            </table>
        </div>
    </body>
</html>