<?php
    include_once('application/config/global.config.php');
    header('Cache-Control: no-cache, no-store, must-revalidate');               // HTTP 1.1 Cache Disabling
    header('Pragma: no-cache');                                                 // HTTP 1.0  Cache Disabling
    header('Expires: 0');                                                       // Proxies Cache Disabling (Probably unnecessary)

//------------------------------------------------------------------------------
//FUNCTIONS --------------------------------------------------------------------
//------------------------------------------------------------------------------
//[c]onvertPressure
/*  Converts Pressure to Metric if needed since database is in imperial
    @params     integer     required        inMg pressure value
    @returns    string                      Converts pressure to mBars */
function convertPressure($Pressure) {
    return number_format($Pressure*33.8639,0,'.','');
}

//[c]onvertTemperature
/*  Converts temperature to Metric if needed since database is in imperial
    @params     integer     required        Fahrenheit temperature
    @returns    string                      Converts Fahrenheit to Metric */
function convertTemperature($Temperature) {
    return number_format(($Temperature-32)/1.8,0,'.','');
}

//[c]reateGraph
/*  This function handles the PHP data then formats it out into the expected Javascript
    format that the Highcharts are expecting.  It's ugly to say the least but the Highcharts
    API refuses to acknowledge existing Stylesheets and expects it all to be inline.
    @params     array       required        The Array of Historical Data
    @params     string      required        The Pre-existing Div ID to inject the graph to (expects #somename)
    @params     string      required        The Graph title (e.g: Pressure over the past 24 hours)
    @params     string      required        The Column Name of the array to use (e.g: 'pressure' == $Data['pressure'](
    @params     integer     required        The minimum value to be displayed on the graph (Calculate before this function)
    @params     integer     required        The maximum value to be displayed on the graph (Calculate before this function)
    @params     string      required        The text to be appened to the end of the mouse-over toolitp (e.g: F, C, mBar, inHG)
    @params     string      required        Hexidecimal color to be used for the data graph
    @params     integer     required        Number of decimals points to use in the average calculation
    @returns    nothing                     Directly injects "<script>...</script>" to the DivID */
function createGraph($Data,$DivID, $Title, $ArrayID, $Min, $Max, $ToolTip, $Color, $AvgDecimals) {
    //Calculate the Average Value for the Data Series
    $TempSum=0;     $TempCount=0;
    foreach($Data AS $Result) {
        $TempSum+=$Result[$ArrayID];
        $TempCount++;
    }
    $AvgValue = number_format(($TempSum / $TempCount),$AvgDecimals,'.','');

    /*  This is straight up idiotic but it works. We're going to create an inline
        script container, dump out some pre-set values in a psuedo json format, then
        loop through an array of SQL records and formatting it out as a series.
        The once the DOM is loaded, the highcharts libraries will go through the
        inline scripts, read the data and manipulate the dom to draw a graph.
        SQL -> PHP -> Inline Javascript -> jQuery -> Graph!  It's Magic
        @TODO: Find a new graphing library/provider
    */
    $sScript =  "<script>" .
                "    $(function(){ $('".$DivID."').highcharts({" .
                "       chart: { " .
                "            type: 'column'," .
                "            backgroundColor: 'transparent', " .                //Lets the Page's CSS handle background
                "            borderColor: '#7f8c8d', " .                        //Yep set this manually (no CSS recognition)
                "        }," .
                "       title: { " .
                "           text: '".$Title."'," .
                "           align: 'left', ".
                "           style: {'color':'#ecf0f1'}" .                       //Title of the graph (no CSS recognition)
                "       },".
                "       legend: {" .
                "           enabled: false,".
                "       },".
                "       xAxis: {" .
                "           lineColor: '#7f8c8d',".                             //The Bottom LINE of the graph (No CSS Recognition)
                "           gridLineColor: '#7f8c8d',".                         //The Grid Color (x)  (No CSS Recognition)
                "           minorGridLineColor: '#7f8c8d',".                    //Minor Grid Colors (x/2)  (No CSS Recognition)
                "           labels: {" .
                "               style: {'color':'#7f8c8d'}, " .                 //Labels on the bottoms  (No CSS Recognition)
                "           }," .
                "           categories: [";
                        for($i=count($Data)-1; $i>=0; $i--) {
                            $sScript .= "'" . date('H:i',$Data[$i]['datestamp']) . "',";
                        }
    $sScript .= "   ]".
                "       },".
                "       yAxis: {".
                "           title: null,".
                "           min: ".$Min.",".
                "           max: ".$Max.",".
                "           plotLines: [{ ".                                    //This draws the "average line"
                "               color: '#f39c12',".                             //Color of the Average line  (No CSS Recognition)
                "               dashStyle: 'dot', " .
                "               value: ".$AvgValue."," .
                "               width: 1," .
                "               zIndex: 3" .
                "           }]," .
                "           gridLineColor: '#7f8c8d'," .                        //Y (Vertical) Grid color  (No CSS Recognition)
                "           minorGridLineColor: '#7f8c8d',".                    //Y/2 (Vertical) grid color  (No CSS Recognition)
                "           labels: {" .
                "               style: {'color':'#7f8c8d'}, " .                 //Labels on the left  (No CSS Recognition)
                "           },".
                "       }," .
                "       tooltip: {" .
                "           shared: true,".
                "           formatter: function() {" .                          //Improper HTML formatting, use ... to offset widths (fake columns... good god)
                "               return 'Time:...... ' + this.x + '<br />Average:. ".$AvgValue.$ToolTip."'+'<br />Value:..... '+ this.y+'".$ToolTip."';" .
                "           }" .
                "       }," .
                "       series: [{" .
                "           name: '".$Title."',".                               //Title of teh data series
                "           color: '".$Color."',".                              //Color of the Graph/Bar/Line (No CSS Recognition)
                "           borderWidth: 0," .                                  //If border is set, draws a white box on our green/blue bars
                "           data: [";
                                for($i=count($Data)-1; $i>=0; $i--) {
                                    $sScript .= number_format($Data[$i][$ArrayID],2,'.','').',';
                                }
    $sScript .= "   ]" .
                "       }]" .
                "       });" .
                "   });" .
                "</script>" . PHP_EOL;

    //Ultra-Basic compression then Echo
    //Data above is heavily spaced for readability. This scripts a lot of excessive spaces
    echo str_replace(', ',',',str_replace('  ', '', str_replace(': ',':',$sScript)));
}

//[g]etMinAndMax
/*  When handed the historical temperature matrix, it calculates the minium and
    maximum value for a desired field.  It also contains previous min/max values so
    that you can compare different arrays.  For example, if we want to show the inside
    and outside graphs with the same min/max, we can send the previous min/max of the inside
    and it'll check to see if the inside values are lower/higher and adjust as needed.
    @params     array       required        The Array of Historical Data
    @params     string      required        The Column name of the array to use (e.g.: 'pressure' == $Data['pressure'])
    @params     integer     required        The Minimum Starting value.
                                                If -1 then find lowest value and set accordingly
                                                If anything else, only update the minimum is if's less than
    @params     integer     required        The maximum starting value
                                                If -1 then find the highest value and set accordingly
                                                If anything else, only update the maximum if it's higher than this
    @returns    array                       array('min'=>integer, 'max'=>integer); */
function getMinAndMax($Data,$Field,$MinStart,$MaxStart){

    $iOutputMin = ($MinStart == '') ? -1 : $MinStart;
    $iOutputMax = ($MaxStart == '') ? -1 : $MaxStart;

    foreach($Data AS $Record) {
        if($iOutputMin == -1 || $Record[$Field] < $iOutputMin) { $iOutputMin = $Record[$Field]; }
        if($iOutputMax == -1 || $Record[$Field] > $iOutputMax) { $iOutputMax = $Record[$Field]; }
    }
    return array('min'=>$iOutputMin,'max'=>$iOutputMax);
}

//------------------------------------------------------------------------------
//LOGIC ------------------------------------------------------------------------
//------------------------------------------------------------------------------

//What Mode are we in? MySQL or SQLite
    switch (DB_TYPE) {
        case DBTYPE_MYSQL:      require_once('application/classes/mysql.class.php');
            $oDB = new MySQLDatabase(DB_HOST,DB_NAME,DB_USER,DB_PASS);
            break;
        case DBTYPE_SQLITE:     require_once('application/classes/sqlite.class.php');
            $oDB = new SQLiteDatabase(DB_PATH);
            break;
        default:                die('Unknown Database Type: '.DB_TYPE);
    }

//Query Back the last 24 Hours
    $sSQL = 'SELECT datestamp, pressure, tempf_inside, tempf_outside, humid_inside, humid_outside ' .
            'FROM history WHERE datestamp > :maxage ORDER BY datestamp ASC ';
    $aHistory = $oDB->query($sSQL,array('maxage' => date('U')-(1*24*60*60+1)));
    unset($oDB);

//If Metric, Convert all the values and Labels over
    if(WEATHER_METRIC) {
        $Labels = array('pressure'=>' mBar','temperature'=>'°C');
        for($i=0;$i<count($aHistory);$i++) {
            $aHistory[$i]['pressure'] = convertPressure($aHistory[$i]['pressure']);
            $aHistory[$i]['tempf_inside'] = convertTemperature($aHistory[$i]['tempf_inside']);
            $aHistory[$i]['tempf_outside'] = convertTemperature($aHistory[$i]['tempf_outside']);
            $Pressure = array('min'=> convertPressure(29.5), 'max'=> convertPressure(30.5),'decimals'=>0);
        }
    } else {
        $Labels = array('pressure'=>' inHg','temperature'=>'°F');
        $Pressure = array('min'=>29.5,'max'=>30.5,'decimals'=>2);
    }

?>
<!html>
<html>
    <head>
        <title>24 Hour Graphs for <?php echo WEATHER_CITY . ', ' . WEATHER_STATE ?></title>
        <meta name="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=0.75" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="author" href="<?php echo SITE_HOME ?>/humans.txt" type="text/plain" />
        <link rel="stylesheet" href="<?php echo SITE_HOME ?>/includes/css/reset.css" />
        <link rel="stylesheet" href="<?php echo SITE_HOME ?>/includes/css/style.css" />
        <script src="<?php echo SITE_HOME ?>/includes/js/zepto.min.js" defer></script>
        <script src="<?php echo SITE_HOME ?>/includes/js/global.min.js" defer></script>
        <script src="<?php echo SITE_HOME ?>/includes/js/graphs/highcharts.js" defer></script>
        <script src="<?php echo SITE_HOME ?>/includes/js/graphs/highcharts-more.js" defer></script>
        <script src="<?php echo SITE_HOME ?>/includes/js/graphs/highcharts-jquery.js"></script>
        <meta name="x-timestamp-page" content="<?php echo date('U') ?>" />
        <meta name="x-timestamp-jquery" content="null" />
        <meta name="x-timestamp-database" content="null" />
    </head>
    <body class="scroll">
        <div id="container" class="graphs">
            <div id="pressure" class="full"></div>
            <div class="cf">
                <div id="temp_outside" class="half fl"></div>
                <div id="temp_inside" class="half fr"></div>
            </div>
            <div class="cf">
                <div id="humid_outside" class="half fl"></div>
                <div id="humid_inside" class="half fr"></div>
            </div>
        </div>
        <?php
            //Pressure Graph (Get data min/max, compare against forced min/max, offset if necessary to create reasonable graphs)
            $aMinMax = getMinAndMax($aHistory,'pressure',-1,-1);
            if($Pressure['min'] < $aMinMax['min']) { $aMinMax['min'] = $Pressure['min'];}
            if($Pressure['max'] > $aMinMax['max']) { $aMinMax['max'] = $Pressure['max'];}
            createGraph($aHistory,'#pressure','Pressure over the past 24 Hours','pressure',$aMinMax['min'],$aMinMax['max'],$Labels['pressure'],"#2980b9",$Pressure['decimals']);

            //Create Temperature Graphs (Use same min/max for in and out)
            $aMinMax = getMinAndMax($aHistory,'tempf_outside',-1,-1);
            $aMinMax = getMinAndMax($aHistory,'tempf_inside',$aMinMax['min'],$aMinMax['max']);
            createGraph($aHistory,'#temp_outside','Temperature Outside', 'tempf_outside',$aMinMax['min']-5,$aMinMax['max']+5,$Labels['temperature'],"#27ae60",1);
            createGraph($aHistory,'#temp_inside','Temperature Inside','tempf_inside',$aMinMax['min']-5,$aMinMax['max']+5,$Labels['temperature'],"#27ae60",1);

            //Create Humidity Graphs (Always between 0 and 100)
            createGraph($aHistory,'#humid_outside','Humidity Outside', 'humid_outside',0,100,'%',"#16a085",0);
            createGraph($aHistory,'#humid_inside','Humidity Inside','humid_inside',0,100,'%',"#16a085",1);
        ?>
    </body>
</html>